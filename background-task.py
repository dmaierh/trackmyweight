#!/usr/bin/env python3
import schedule
import time
import os

def job():
    os.system("python nokia-weight-sync.py sync garmin")

schedule.every(30).seconds.do(job)

while 1:
    schedule.run_pending()
    time.sleep(1)
